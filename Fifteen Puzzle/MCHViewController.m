//
//  MCHViewController.m
//  Fifteen Puzzle
//

#import "MCHViewController.h"

#import "MCHFifteenPuzzle.h"
#import "MCHGridPosition.h"
#import "MCHPuzzleView.h"
#import "MCHPuzzleTileView.h"




@interface MCHViewController_PuzzleObserver : NSObject <MCHFifteenPuzzleObserver>
- (instancetype)init __unavailable;
@end

@implementation MCHViewController_PuzzleObserver
{
    __weak MCHPuzzleView* _puzzleView;
}

- (instancetype)initWithPuzzleView:(MCHPuzzleView*)puzzleView
{
    self = [super init];
    if (self) {
        _puzzleView = puzzleView;
    }
    return self;
}

- (void)puzzleContentsChanged:(NSDictionary*)changeSet
{
    MCHPuzzleView* puzzleView = _puzzleView;
    [changeSet enumerateKeysAndObjectsUsingBlock:
        ^(MCHGridPosition* position, NSNumber* newContents, __unused BOOL* stop)
    {
        NSString* contentsString = @"";
        if ([newContents isKindOfClass:[NSNumber class]]) {
            contentsString = [newContents stringValue];
        }
        [[puzzleView tileViewInRow:position.row column:position.column]
         setContents:contentsString];
    }];
}

- (void)puzzleTileAtPosition:(MCHGridPosition*)oldPosition
                     movedTo:(MCHGridPosition*)newPosition
{
    MCHPuzzleView* puzzleView = _puzzleView;
    MCHPuzzleTileView* slidingTileView =
        [puzzleView tileViewInRow:oldPosition.row column:oldPosition.column];
    MCHPuzzleTileView* spaceView =
        [puzzleView tileViewInRow:newPosition.row column:newPosition.column];
    CGPoint slidingTileOriginalCenter = slidingTileView.center;
    [UIView animateWithDuration:0.2 animations:^{
        slidingTileView.center = spaceView.center;
    } completion:^(__unused BOOL finished) {
//        BOOL savedAnimationEnablementState = [UIView areAnimationsEnabled];
//        [UIView setAnimationsEnabled:NO];
        spaceView.contents = slidingTileView.contents;
        slidingTileView.contents = @"";
        slidingTileView.center = slidingTileOriginalCenter;
//        [UIView setAnimationsEnabled:savedAnimationEnablementState];
    }];

}

@end




@implementation MCHViewController
{
    MCHViewController_PuzzleObserver* _puzzleObserver;
}


#pragma mark - View Lifecycle


//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//    // Do any additional setup after loading the view, typically from a nib.
//}

- (void)viewWillAppear:(__unused BOOL)animated
{
    [super viewWillAppear:animated];
    _puzzleObserver = [[MCHViewController_PuzzleObserver alloc]
                       initWithPuzzleView:self.puzzleView];
    [self.puzzle addPuzzleObserver:_puzzleObserver];
}

- (void)viewDidDisappear:(__unused BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.puzzle removePuzzleObserver:_puzzleObserver];
    _puzzleObserver = nil;
}

//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}



#pragma mark - UI Actions


- (IBAction)tileTapped:(MCHPuzzleTileView*)sender {
    MCHGridPosition* tappedTilePosition = [[MCHGridPosition alloc]
                                           initWithRow:sender.tileRow
                                                column:sender.tileColumn];
    [self.puzzle slideTileAtPosition:tappedTilePosition];
}

@end
