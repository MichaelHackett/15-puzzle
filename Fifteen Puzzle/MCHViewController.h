//
//  MCHViewController.h
//  Fifteen Puzzle
//

#import <UIKit/UIKit.h>

@class MCHFifteenPuzzle;
@class MCHPuzzleTileView;
@class MCHPuzzleView;



@interface MCHViewController : UIViewController

@property (strong, nonatomic) MCHFifteenPuzzle* puzzle;
@property (strong, nonatomic) NSNotificationCenter* notificationSource;

// Set automatically when storyboard/NIB loaded:
@property (weak, nonatomic) IBOutlet MCHPuzzleView* puzzleView;

- (IBAction)tileTapped:(MCHPuzzleTileView*)sender;

@end
