//
//  MCHPuzzleView.h
//  Fifteen Puzzle
//

#import <UIKit/UIKit.h>

@class MCHPuzzleTileView;


@interface MCHPuzzleView : UIView

- (MCHPuzzleTileView*)tileViewInRow:(NSUInteger)row
                             column:(NSUInteger)column;

@end
