//
//  MCHGridPosition.h
//  Fifteen Puzzle
//

#import <Foundation/Foundation.h>

@interface MCHGridPosition : NSObject <NSCopying>

@property (assign, readonly, nonatomic) NSUInteger row;
@property (assign, readonly, nonatomic) NSUInteger column;

- (instancetype)initWithRow:(NSUInteger)row
                     column:(NSUInteger)column;

- (BOOL)isEqualToGridPosition:(MCHGridPosition*)otherPosition;
- (BOOL)isAdjacentTo:(MCHGridPosition*)otherPosition;

@end
