//
//  MCHPuzzleView.m
//  Fifteen Puzzle
//

#import "MCHPuzzleView.h"

#import "MCHPuzzleTileView.h"



@implementation MCHPuzzleView

- (NSArray*)tileViews
{
    NSMutableArray* tileViews = [[NSMutableArray alloc] init];
    for (UIView* subview in self.subviews) {
        if ([subview isKindOfClass:[MCHPuzzleTileView class]]) {
            [tileViews addObject:subview];
        }
    }
    return [tileViews copy];
}

- (MCHPuzzleTileView*)tileViewInRow:(NSUInteger)row
                             column:(NSUInteger)column
{
    for (MCHPuzzleTileView* tileView in self.tileViews) {
        if ((tileView.tileRow == row) && (tileView.tileColumn == column)) {
            return tileView;
        }
    }
    return nil;
}

@end
