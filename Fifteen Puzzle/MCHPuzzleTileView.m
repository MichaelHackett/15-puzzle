//
//  MCHPuzzleTileView.m
//  Fifteen Puzzle
//

#import "MCHPuzzleTileView.h"



// Note: The various custom setters in this class are not KVC/KVO-compliant,
// but it would be rare to watch the state of a view. This can be added if
// needed.

@implementation MCHPuzzleTileView

- (void)setSelected:(BOOL)selected
{
    if (selected) {
        self.backgroundColor = self.tintColor;
        [self setTitleColor:[self titleColorForState:UIControlStateSelected]
                   forState:UIControlStateNormal];
    } else {
        self.backgroundColor = [UIColor clearColor];
        [self setTitleColor:self.tintColor
                   forState:UIControlStateNormal];
    }
}

- (NSString*)contents
{
    return [self titleForState:UIControlStateNormal];
}

- (void)setContents:(NSString*)contents
{
    [self setTitle:contents forState:UIControlStateNormal];
}

@end
