//
//  main.m
//  Fifteen Puzzle
//

#import <UIKit/UIKit.h>

#import "MCHAppDelegate.h"

int main(int argc, char* argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MCHAppDelegate class]));
    }
}
