//
//  MCHFifteenPuzzle.h
//  Fifteen Puzzle
//
//  The puzzle uses 1-based row and column indices; that is, the top-left
//  grid position is (1,1). A position with either index set to 0 is considered
//  invalid, and may result in an exception being thrown.
//
//  The "space" in the puzzle is represented by a nil value for the contents
//  of the tile at that position.

#import <Foundation/Foundation.h>

@class MCHGridPosition;


// Observer protocol

@protocol MCHFifteenPuzzleObserver <NSObject>

- (void)puzzleContentsChanged:(NSDictionary*)changeSet;  // MCHGridPosition -> NSNumber
- (void)puzzleTileAtPosition:(MCHGridPosition*)oldPosition
                     movedTo:(MCHGridPosition*)newPosition;

@end




@interface MCHFifteenPuzzle : NSObject

- (NSNumber*)contentsOfTileAtPosition:(MCHGridPosition*)tilePosition;
- (void)slideTileAtPosition:(MCHGridPosition*)positionOfTileToSlide;

- (void)addPuzzleObserver:(id<MCHFifteenPuzzleObserver>)observer;
- (void)removePuzzleObserver:(id<MCHFifteenPuzzleObserver>)observer;

@end
