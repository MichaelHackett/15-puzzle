//
//  MCHFifteenPuzzle.m
//  Fifteen Puzzle
//

#import "MCHFifteenPuzzle.h"

#import "MCHGridPosition.h"


static NSUInteger const GRID_WIDTH = 4;
static NSUInteger const GRID_HEIGHT = 4;



// Convert from 2D coordinates to 1D array index (assuming row major layout).
static NSUInteger tileIndexFromPosition(MCHGridPosition* position) {
    return (position.row - 1) * GRID_WIDTH + (position.column - 1);
}




@implementation MCHFifteenPuzzle
{
    MCHGridPosition* _spacePosition;
    NSMutableArray* _tileContents;
    NSMutableArray* _observers;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _observers = [[NSMutableArray alloc] init];
        _tileContents = [NSMutableArray arrayWithCapacity:GRID_HEIGHT * GRID_WIDTH];
        for (NSUInteger row = 1; row <= GRID_HEIGHT; row++) {
            for (NSUInteger column = 1; column <= GRID_WIDTH; column++) {
                MCHGridPosition* position = [[MCHGridPosition alloc]
                                             initWithRow:row column:column];
                _tileContents[tileIndexFromPosition(position)] =
                        @((row - 1) * GRID_WIDTH + column);
            }
        }
        // "Remove" the last tile to create the space.
        _spacePosition = [[MCHGridPosition alloc]
                          initWithRow:GRID_HEIGHT
                               column:GRID_WIDTH];
        _tileContents[tileIndexFromPosition(_spacePosition)] = [NSNull null];
    }
    return self;
}



#pragma mark - Observer management


// When an observer is added, it is automatically sent an initial set of
// messages to give it the current state of all observable properties.

- (void)addPuzzleObserver:(id<MCHFifteenPuzzleObserver>)observer
{
    [_observers addObject:observer];

    NSMutableDictionary* changeSet = [[NSMutableDictionary alloc] init];
    for (NSUInteger row = 1; row <= GRID_HEIGHT; row++) {
        for (NSUInteger column = 1; column <= GRID_WIDTH; column++) {
            MCHGridPosition* position = [[MCHGridPosition alloc]
                                         initWithRow:row column:column];
            [changeSet setObject:_tileContents[tileIndexFromPosition(position)]
                          forKey:position];
        }
    }
    [observer puzzleContentsChanged:changeSet];
}

- (void)removePuzzleObserver:(id<MCHFifteenPuzzleObserver>)observer
{
    [_observers removeObject:observer];
}




#pragma mark - Puzzle state

- (void)MCHFifteenPuzzle_validateTilePosition:(MCHGridPosition*)tilePosition
{
    if (tilePosition.row < 1 || tilePosition.row > GRID_HEIGHT) {
        [NSException raise:NSInvalidArgumentException
                    format:@"row number must be in range 1-%d", GRID_HEIGHT];
    }
    if (tilePosition.column < 1 || tilePosition.column > GRID_WIDTH) {
        [NSException raise:NSInvalidArgumentException
                    format:@"column number must be in range 1-%d", GRID_WIDTH];
    }
}

- (NSNumber*)contentsOfTileAtPosition:(MCHGridPosition*)tilePosition
{
    [self MCHFifteenPuzzle_validateTilePosition:tilePosition];
    id contents = _tileContents[tileIndexFromPosition(tilePosition)];
    if (contents == [NSNull null]) {
        contents = nil;
    }
    return contents;
}

- (void)slideTileAtPosition:(MCHGridPosition*)positionOfTileToSlide
{
    [self MCHFifteenPuzzle_validateTilePosition:positionOfTileToSlide];
    if ([positionOfTileToSlide isAdjacentTo:_spacePosition]) {
        NSUInteger spaceIndex = tileIndexFromPosition(_spacePosition);
        NSUInteger tileIndex = tileIndexFromPosition(positionOfTileToSlide);
        _tileContents[spaceIndex] = _tileContents[tileIndex];
        _tileContents[tileIndex] = [NSNull null];

        for (id<MCHFifteenPuzzleObserver> observer in _observers) {
            [observer puzzleTileAtPosition:positionOfTileToSlide
                                   movedTo:_spacePosition];
        }

        _spacePosition = positionOfTileToSlide;
    }
}

@end
