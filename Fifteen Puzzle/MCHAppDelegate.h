//
//  MCHAppDelegate.h
//  Fifteen Puzzle
//

#import <UIKit/UIKit.h>

@interface MCHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow* window;

@end
