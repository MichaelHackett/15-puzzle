//
//  MCHGridPosition.m
//  Fifteen Puzzle
//

#import "MCHGridPosition.h"


@implementation MCHGridPosition

- (instancetype)initWithRow:(NSUInteger)row
                     column:(NSUInteger)column
{
    self = [super init];
    if (self) {
        _row = row;
        _column = column;
    }
    return self;
}

- (BOOL)isEqual:(id)object
{
    if ([object isKindOfClass:[MCHGridPosition class]]) {
        return [self isEqualToGridPosition:(MCHGridPosition*)object];
    }
    return NO;
}

- (BOOL)isEqualToGridPosition:(MCHGridPosition*)otherPosition
{
    return (self.row == otherPosition.row) && (self.column == otherPosition.column);
}

- (BOOL)isAdjacentTo:(MCHGridPosition*)otherPosition
{
    // The positions are adjacent if either rows are equal and columns are 1
    // apart or vice-versa, i.e., the sum of the absolute deltas must be 1.
    return (abs((int)self.row - (int)otherPosition.row) +
            abs((int)self.column - (int)otherPosition.column)) == 1;
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"(%u,%u)", self.row, self.column];
}

- (id)copyWithZone:(__unused NSZone*)zone
{
    // Instances of MCHGridPosition are immutable so just return input reference
    // with +1 retain count.
    if ([self isMemberOfClass:[MCHGridPosition class]]) {
        return self;
    }
    // Else, make a fresh instance with the same values.
    return [[[self class] alloc] initWithRow:self.row column:self.column];
}

@end
