//
//  MCHPuzzleTileView.h
//  Fifteen Puzzle
//

#import <UIKit/UIKit.h>



@interface MCHPuzzleTileView : UIButton

@property (assign, nonatomic) NSUInteger tileRow;
@property (assign, nonatomic) NSUInteger tileColumn;

@property (copy, nonatomic) NSString* contents;

@end
